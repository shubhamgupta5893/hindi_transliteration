# -*- coding: utf-8 -*-
from __future__ import print_function
import re
from mappings import conversiontable
consonants = re.compile(ur'[\u0915-\u0939\u0958-\u095F\u0978-\u097C\u097E-\u097F]')
vowelsigns = re.compile(ur'[\u093E-\u094C\u093A-\u093B\u094E-\u094F\u0955-\u0957]')
nukta = re.compile(ur'[\u093C]')
virama = re.compile(ur'[\u094D]')

devanagarichars = re.compile(ur'[\u0900-\u097F\u1CD0-\u1CFF\uA8E0-\uA8FF]')

def deva_to_latn(text):

    word = text.strip()

    curr = ''

    #for index, char in enumerate(word):
    index = 0
    while index < len(word):
        char = word[index]
        if devanagarichars.match(char):

            if consonants.match(char):

                if index+1 < len(word):
                    nextchar = word[index+1]
                    if nukta.match(nextchar):
                        cons = char + nextchar
                        nukta_present = 1
                        index += 1
                    else:
                        cons = char
                        nukta_present = 0
                    
                    nextnextchar  = None
                    if index+2 < len(word):
                        nextnextchar = word[index+2]
                    if nukta_present and nextnextchar is not None:
                        if vowelsigns.match(nextnextchar) or virama.match(nextnextchar):
                            trans = conversiontable.get(cons, '')
                            curr = curr + trans
                        else:
                            trans = conversiontable.get(cons, '')
                            trans = trans + "a"
                            curr = curr + trans
                    else:
                        trans = conversiontable.get(cons, '')
                        curr = curr + trans 
                        if consonants.match(nextchar):
                                curr += "a"
                                
                                
                else:
                    trans= conversiontable.get(char, '')
                    curr = curr+trans
            else:
                trans = conversiontable.get(char, '')
                curr = curr + trans


        index += 1

    return curr


def getLatin(inputtext):

    word_syllables = []
    all_words = []

    for sen in inputtext.split("\n"):
        for word in sen.split():
            latin_output = deva_to_latn(word)
            all_words.append(latin_output)
        all_words.append("\n")
    joined_all_words = ' '.join(all_words)

    return joined_all_words


if __name__ == "__main__":
    text = u"उन्हें बुद्धि और अन्तरात्मा की देन प्राप्त है और परस्पर उन्हें भाईचारे के भाव से बर्ताव करना चाहिए"
    text = u'''
इश्क़ के मराहिल में वो भी वक़्त आता है 

आफ़तें बरसती हैं दिल सुकून पाता है 

आज़माइशें ऐ दिल सख़्त ही सही लेकिन 

ये नसीब क्या कम है कोई आज़माता है 

उम्र जितनी बढ़ती है और घटती जाती है 

साँस जो भी आता है लाश बन के जाता है 

आबलों का शिकवा क्या ठोकरों का ग़म कैसा 

आदमी मोहब्बत में सब को भूल जाता है 

कार-ज़ार-ए-हस्ती में इज़्ज़-ओ-जाह की दौलत 

भीक भी नहीं मिलती आदमी कमाता है 

अपनी क़ब्र में तन्हा आज तक गया है कौन 

दफ़्तर-ए-अमल 'आमिर' साथ साथ जाता है 
    '''
    
    #text = u'''मुझ को अपने दिल-ए-नाकाम पे रोना आया''' 
    #text = u'उन्हें बुद्धि'

    print(text)
    print("after conversing")
    print(getLatin(text))
